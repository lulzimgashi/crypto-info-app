import { AppRegistry } from 'react-native';

import React from 'react';
import NativeRouter from './App';

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import reducers from './redux/reducers';


export const store = createStore(
    reducers,
    applyMiddleware(thunk)
)

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('cryptovalues', () => App);
