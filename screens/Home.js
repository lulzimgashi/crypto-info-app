//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { refreshCryptos } from '../redux/actions';
//components
import HeaderItem from '../components/HeaderItem';
import ListItem from '../components/ListItem';
import Loading from '../components/Loading';

// create a component
class Home extends Component {
    state = { cryptoList: [] }
    renderItem = (item) => {
        return <ListItem value={this.props.value} item={item} onPress={() => this.itemPress(item)} />
    }


    componentWillMount() {
        this.setState({ cryptoList: this.props.cryptoList })
    }


    componentDidMount() {
        if ((this.props.cryptoList.length === 0 || this.props.valueChanged) && this.props.value !== 'none') {
            this.props.dispatch(refreshCryptos(this.props.value));
        }
    }


    componentWillReceiveProps(nextProps) {
        this.setState({ cryptoList: nextProps.cryptoList });
    }


    itemPress = (item) => {
        this.props.history.push('/details', { item });
    }

    leftPress = () => {
        this.props.history.push('/settings');
    }

    rightPress = () => {
        this.props.dispatch(refreshCryptos(this.props.value));
    }

    getItemLayout = (data, index) => (
        { length: 50, offset: 50 * index, index }
    )

    render() {
        return (
            <View style={styles.container}>
                <HeaderItem leftIcon='cogs' leftPress={this.leftPress} rightIcon='refresh' rightPress={this.rightPress} title='Home' />
                {this.props.value === 'none' ? <Text style={styles.warning}>Please chose any value in settings</Text> : null}
                {this.props.loading ? <Loading /> :
                    <FlatList
                        data={this.state.cryptoList}
                        renderItem={({ item }) => this.renderItem(item)}
                        keyExtractor={(item, index) => index.toString()}
                        getItemLayout={this.getItemLayout} />}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    warning: {
        textAlign: 'center',
        marginTop: 20
    }
});


const mapStateToProps = state => {
    return { value: state.value, cryptoList: state.cryptoList, loading: state.loading, valueChanged: state.valueChanged };
}

export default connect(mapStateToProps)(Home);
