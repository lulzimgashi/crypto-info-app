//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { refreshCrypto } from '../redux/actions';
//components
import HeaderItem from '../components/HeaderItem';
import Loading from '../components/Loading';

// create a component
class CryptoDetails extends Component {
    state = { loading: false };

    leftPress = () => {
        this.props.history.goBack();
    }

    componentWillMount() {
        this.setState({ item: this.props.history.location.state.item });
    }

    rightPress = () => {
        this.setState({ loading: true });
        refreshCrypto(this.state.item.id, this.props.value, response => {
            if (response[0]) {
                this.setState({ item: response[0], loading: false });
            } else {
                this.setState({ loading: false });
            }
        })
    }

    render() {
        const item = this.state.item;
        return (
            <View style={styles.container} >
                <HeaderItem leftIcon='arrow-left' leftPress={this.leftPress} rightIcon='refresh' rightPress={this.rightPress} title='Details' />
                <View style={styles.row}>
                    <View style={styles.symbol}><Text style={styles.symbolText}>{item.symbol}</Text></View>
                    {this.state.loading ? <Loading /> :
                        <Text style={styles.name}>{item.name}</Text>}
                    <View style={styles.symbol}><Text style={styles.symbolText}>{item.rank}</Text></View>
                </View>

                <View style={styles.divider} />

                <View>
                    <View style={styles.row}>
                        <Text style={styles.category}>Price {this.props.value}</Text>
                        <Text>{item['price_' + this.props.value.toLowerCase()]}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.category}>Price BTC</Text>
                        <Text>{item.price_btc}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.category}>Available Supply</Text>
                        <Text>{item.available_supply}</Text>
                    </View>
                </View>

                <View style={styles.divider} />

                <View>
                    <View style={styles.row}>
                        <Text style={styles.category}>1h Change</Text>
                        <Text>{item.percent_change_1h}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.category}>24h Change</Text>
                        <Text>{item.percent_change_24h}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.category}>7d Change</Text>
                        <Text>{item.percent_change_7d}</Text>
                    </View>
                </View>


            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 10
    },
    symbol: {
        width: 50,
        height: 50,
        backgroundColor: 'rgb(217,77,67)',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    symbolText: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
    },
    name: {
        fontSize: 18,
        backgroundColor: 'rgb(217,77,67)',
        color: 'white',
        padding: 5,
        borderRadius: 5,
        overflow: 'hidden'
    },
    category: {
        fontSize: 12,
        backgroundColor: 'rgb(217,77,67)',
        color: 'white',
        padding: 5,
        borderRadius: 5,
        overflow: 'hidden'
    },
    divider: {
        height: 1,
        backgroundColor: 'rgb(217,77,67)',
        margin: 20,
        marginLeft: 50,
        marginRight: 50
    }
});

const mapStateToProps = state => {
    return { value: state.value };
}

export default connect(mapStateToProps)(CryptoDetails);
