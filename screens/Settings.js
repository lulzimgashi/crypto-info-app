//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { changeValue } from '../redux/actions';
//components
import HeaderItem from '../components/HeaderItem';
import ListItem from '../components/ListItem';

// create a component
class Settings extends Component {

    state = { value: '' }

    componentWillMount() {
        this.setState({ value: this.props.value });
    }

    changeValue = (value) => {
        this.props.dispatch(changeValue(value));
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.value !== nextProps.value) {
            this.setState({ value: nextProps.value })
        }
    }

    onLeftPress = () => {
        this.props.history.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <HeaderItem leftIcon='arrow-left' leftPress={this.onLeftPress} title='Currencies' />
                <View style={styles.optionsContainer}>
                    <Text onPress={() => this.changeValue('USD')} style={this.state.value === 'USD' ? styles.selected : styles.notSelected}>USD</Text>
                    <Text onPress={() => this.changeValue('EUR')} style={this.state.value === 'EUR' ? styles.selected : styles.notSelected}>EUR</Text>
                    <Text onPress={() => this.changeValue('CNY')} style={this.state.value === 'CNY' ? styles.selected : styles.notSelected}>CNY</Text>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    selected: {
        fontSize: 18,
        backgroundColor: 'rgb(217,77,67)',
        color: 'white',
        padding: 5,
        borderRadius: 5,
        overflow: 'hidden'
    },
    notSelected: {
        fontSize: 18,
        color: 'rgb(217,77,67)'
    },
    optionsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        flex: 1
    }
});


const mapStateToProps = state => {
    return { value: state.value }
}

export default connect(mapStateToProps)(Settings);
