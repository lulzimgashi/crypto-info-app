//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,ActivityIndicator } from 'react-native';

// create a component
class Loading extends Component {
    render() {
        return (
            <ActivityIndicator size="large" color="rgb(217,77,67)" style={styles.indicator} />
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    indicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});

//make this component available to the app
export default Loading;
