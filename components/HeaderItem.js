//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

// create a component
class Header extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Icon name={this.props.leftIcon} onPress={this.props.leftPress} size={18} color="white" />
                <Text style={styles.title}>{this.props.title}</Text>
                <Icon name={this.props.rightIcon} onPress={this.props.rightPress} size={18} color="white" />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: 'rgb(217,77,67)'
    },
    title: {
        fontWeight: 'bold',
        color: 'white'
    },
    option: {
        fontSize: 12
    }
});

//make this component available to the app
export default Header;
