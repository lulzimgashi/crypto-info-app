//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

// create a component
class ListItem extends Component {
    render() {
        const item = this.props.item;
        return (
            <TouchableOpacity onPress={this.props.onPress}>
                <View style={styles.container}>
                    <View style={styles.symbol}><Text style={styles.symbolText}>{item.symbol}</Text></View>
                    <View style={styles.valuesContainer}>
                        <Text>{item['price_' + this.props.value.toLowerCase()]}</Text>
                        <Text>{item.percent_change_24h}</Text>
                    </View>
                    <Text style={styles.rankText}>{item.rank}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        height: 70,
        borderBottomColor: 'rgb(217,77,67)',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5
    },
    symbol: {
        width: 50,
        height: 50,
        backgroundColor: 'rgb(217,77,67)',
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center'
    },
    symbolText: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold'
    },
    valuesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 40
    },
    rankText: {
        color: 'rgb(217,77,67)',
        fontSize: 14,
        fontWeight: 'bold'
    }
});

//make this component available to the app
export default ListItem;
