import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { NativeRouter, Route, Link, Switch } from 'react-router-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { connect } from 'react-redux';
//actions
import { getValue } from './redux/actions'

//components
import Home from './screens/Home';
import CryptoDetails from './screens/CryptoDetails';
import Settings from './screens/Settings';
import Loading from './components/Loading';

class App extends Component {

  state = { value: null }

  componentWillMount() {
    this.props.dispatch(getValue());
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.value !== nextProps.value) {
      this.setState({ value: nextProps.value });
    }
  }


  render() {
    return (
      <NativeRouter>
        <View style={styles.container}>
          {!this.state.value ?
            <Loading /> :
            <Switch>
              <Route exact path="/details" component={CryptoDetails} />
              <Route exact path="/settings" component={Settings} />
              <Route component={Home} />
            </Switch>}
        </View>
      </NativeRouter>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: getStatusBarHeight(true)
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});


const mapStateToProps = state => {
  return { value: state.value }
}

export default connect(mapStateToProps)(App);

