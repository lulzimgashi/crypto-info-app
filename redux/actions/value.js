import { VALUE_CHANGED, VALUE_CHANGE } from './types';

import { AsyncStorage } from 'react-native';


export const changeValue = value => dispatch => {
    dispatch({ type: VALUE_CHANGE, payload: value });
    AsyncStorage.setItem('value', value);
}

export const getValue = () => dispatch => {
    AsyncStorage.getItem('value').then(value => {
        if (value) {
            dispatch({ type: VALUE_CHANGE, payload: value })
        } else {
            dispatch({ type: VALUE_CHANGE, payload: 'none' })
        }
    });
}