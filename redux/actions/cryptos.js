import { CRYPTO_LIST_CHANGED, LOADING_TRUE, LOADING_FALSE } from './types';
import { API } from '../../configuration';
import axios from 'axios';

export const refreshCryptos = value => dispatch => {
    dispatch({ type: LOADING_TRUE });
    axios.get(API + 'ticker/?convert=' + value + '&limit=100')
        .then(response => {
            dispatch({ type: CRYPTO_LIST_CHANGED, payload: response.data });
        })
        .catch(error => {
            dispatch({ type: LOADING_FALSE });
        })
}

export const refreshCrypto = (id, value, callback) => {

    axios.get(API + 'ticker/' + id + '/?convert=' + value)
        .then(response => {
            callback(response.data);
        })
        .catch(error => {
            callback();
        })
}