import { LOADING_TRUE, LOADING_FALSE, CRYPTO_LIST_CHANGED,VALUE_CHANGE } from '../actions/types';

export default (state = false, action) => {
    switch (action.type) {
        case LOADING_TRUE:
            return true;
        case LOADING_FALSE:
            return false;
        case CRYPTO_LIST_CHANGED:
            return false;
        case VALUE_CHANGE:
            return false;
        default:
            return state;
    }
}