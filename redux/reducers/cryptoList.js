import { CRYPTO_LIST_CHANGED } from '../actions/types';

export default (state = [], action) => {
    switch (action.type) {
        case CRYPTO_LIST_CHANGED:
            return action.payload;
        default:
            return state;
    }
}