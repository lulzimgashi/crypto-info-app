import { VALUE_CHANGE, CRYPTO_LIST_CHANGED } from '../actions/types';

export default (state = false, action) => {
    switch (action.type) {
        case VALUE_CHANGE:
            return true;
        case CRYPTO_LIST_CHANGED:
            return false;
        default:
            return state;
    }
}