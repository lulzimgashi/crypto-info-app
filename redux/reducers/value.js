import { VALUE_CHANGE } from '../actions/types';

export default (state = null, action) => {
    switch (action.type) {
        case VALUE_CHANGE:
            return action.payload
        default:
            return state;
    }
}