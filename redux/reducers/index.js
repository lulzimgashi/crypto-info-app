import { combineReducers } from 'redux'
import value from './value';
import cryptoList from './cryptoList';
import loading from './loading';
import valueChanged from './valueChanged';

const rootReducer = combineReducers({
    value, cryptoList, loading, valueChanged
});

export default rootReducer;